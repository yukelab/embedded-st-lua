#ifndef LUA_MODULES_PACKAGE_MANAGER_H
#define LUA_MODULES_PACKAGE_MANAGER_H

#include "lua_modules/common.h"

#define LUA_MODULES_PACKAGE_MANAGER_LIB_NAME "package_manager"

int luaopen_package_manager(lua_State *L);

#endif /* LUA_MODULES_PACKAGE_MANAGER_H */