#include "lua_modules/system.h"

#include <string.h>

#include "lua_modules/common.h"
#include "FreeRTOS.h"
#include "rotable.h"
#include "cmsis_os.h"

jmp_buf system_termination_buffer;
char * selected_application = NULL;

static int yield(lua_State *L)
{
    (void)L;
    taskYIELD();
    return 0;
}

static int delay(lua_State *L)
{
    const uint32_t tt = luaL_checkinteger(L, 1);

    osDelay(tt);

    return 0;
}

static int get_milis(lua_State *L)
{
    int ms = osKernelSysTick() * portTICK_PERIOD_MS;

    lua_pushinteger(L, ms);

    return 1;
}

static int get_seconds(lua_State *L)
{
    TickType_t ticks = osKernelSysTick();
    double seconds = ((double)ticks * portTICK_PERIOD_MS / 1000.0);

    lua_pushnumber(L, seconds);

    return 1;
}

static int launch_application(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 1);

    const char * app_name = luaL_checkstring(L, 1);

    if(selected_application) {
        vPortFree(selected_application);
    }
    selected_application = pvPortMalloc(strlen(app_name) + 1);

    strcpy(selected_application, app_name);

    longjmp(system_termination_buffer, 1);

    return 0;
}

static const luaL_Reg lib[] = {
    {"yield", yield},
    {"delay", delay},
    {"get_milis", get_milis},
    {"get_seconds", get_seconds},

    {"launch_application", launch_application},

    {NULL, NULL}};

int luaopen_system(lua_State *L) {
	rotable_newlib(L, lib);
	return 1;
}