import os

from pprint import pprint

DEVICES_CONFIGURATION = [
    {
        'target_name': 'STM32F746G-DISCO',
        'modules': [
            {'path': 'Core/SConscript'},
            {'path': 'Drivers/SConscript'},
            {'path': 'FATFS/SConscript'},
            {'path': 'LWIP/SConscript'},
            {'path': 'Middlewares/SConscript'},
        ]
    }
]

def _extend_dict(_first_dict, _second_dict):
    """ Extend values in dictionary """
    _dict = {}
    for iterable in _first_dict.keys():
        _dict[iterable] = _first_dict[iterable]

    for iterable in _second_dict.keys():
        if iterable not in _dict:
            _dict[iterable] = _second_dict[iterable]

        elif isinstance(_second_dict[iterable], list):
            _dict[iterable] += _second_dict[iterable]

        elif isinstance(_second_dict[iterable], dict):
            _dict[iterable].update(_second_dict[iterable])

    return _dict

def _extend_env_by_variables(env, _dict, fields_to_extract):
    for item in fields_to_extract:
        if item in _dict:
            if item in env:
                if type(_dict[item]) == dict:
                    env[item].update(_dict[item])
                else:
                    env[item] += _dict[item]
            else:
                env[item] = _dict[item]
    return env

def build_firmware(target_name, modules, **kwargs):
    build_type = 'release' if GetOption('release_build') else 'debug'
    build_dir = os.path.join(Dir('.').abspath, target_name, build_type)
    source_dir = Dir('..').abspath
    
    target = target_name

    global_build_flags = SConscript('./SConscript_flags')
    modules_list = [SConscript(module['path']) for module in modules]

    env = Environment(env=os.environ,
                      tools=['gcc', 'g++', 'gnulink',
                             'ar', 'as', 'arm-none-eabi', 'compile_commands'])

    if not GetOption("verbose_flag") and not GetOption('compile_commands'):
        env['CCCOMSTR'] = "Building $TARGET"
        env['CXXCOMSTR'] = "Building $TARGET"
        env['LINKCOMSTR'] = "Linking $TARGET"
        env['ASCOMSTR'] = "Building $TARGET"

    build_dictionary = {}

    if target_name in global_build_flags:
        if 'default' in global_build_flags[target_name]:
            build_dictionary = _extend_dict(build_dictionary, global_build_flags[target_name]['default'])
        if build_type in global_build_flags[target_name]:
            build_dictionary = _extend_dict(build_dictionary, global_build_flags[target_name][build_type])

    for module in modules_list:
        for section in ['common', target_name]:
            if section in module:
                build_dictionary = _extend_dict(build_dictionary, module[section])
    
    env.VariantDir(build_dir, source_dir, duplicate=False)

    FIELDS_TO_EXTRACT = ["LIBS", "LIBPATH", "LINKFLAGS", "CPPDEFINES", "CPPPATH", "CFLAGS", "CXXFLAGS", "ASFLAGS"]

    env = _extend_env_by_variables(env, build_dictionary, FIELDS_TO_EXTRACT)

    if build_type in build_dictionary:
        env = _extend_env_by_variables(env, build_dictionary[build_type], FIELDS_TO_EXTRACT)
    
    env["LINKFLAGS"] += ['-Xlinker', '-Map="' +
                         os.path.join(build_dir, target) + '.map"']


    sources = [os.path.join(build_dir, os.path.relpath(source, source_dir)) for source in build_dictionary["sources"]]

    elf_artifact = env.Program(source=sources, target=os.path.join(build_dir, target))

    env.Clean(elf_artifact, os.path.join(build_dir, target) + '.map')
    env.Clean(elf_artifact, os.path.join(build_dir, target))

    binary_artifact = env.Binary(source=elf_artifact, target=os.path.join(build_dir, target))

    Default(binary_artifact)

    if GetOption('compile_commands'):
        env.Tool('compile_commands')
        compile_base = env.CompileCommands(build_dir)

        compile_commands_path = os.path.relpath(build_dir, source_dir) + '/compile_commands.json'

        if env.GetOption('clean'):
            env.Clean(compile_commands_path, compile_commands_path)

        if BUILD_TARGETS and compile_base not in BUILD_TARGETS:
            BUILD_TARGETS.append(compile_commands_path)

def main():
    for configuration in DEVICES_CONFIGURATION:
        build_firmware(**configuration)

main()
