package.path = '/system/startup_modules/?.lua;' .. package.path

lcd = require "lcd"
pkg = require "package_manager"
ts = require "ts"
netif = require "network_interface"
system = require "system"
btn = require "button"
mqtt = require "mqtt_client"
gui = require "gui"

CONFIG = {
    pkg = {
        dirs = {
            packages = '/sd/system/packages',
            apps = '/sd/system/apps'
        },
        proto_mqtt = {
            client = { id = "lua-c1" },
            server = { ip = "10.42.0.1", port = 1883 }
        }
    },
    network = {
        use_dhcp = true,
        static = {
            ip = "192.168.0.201",
            mask = "255.255.255.0",
            gateway = "192.168.0.1"
        }
    }
}

local tab = "apps"

local apps_list = {}
local packages_list = {}
local pkg_list_context = { elements = {}}
local app_list_context = { elements = {}}

local client = mqtt.new()

function init_display()
   lcd.init()
    lcd.set_display_on_off(true)
    local ss = lcd.get_screen_size()
    ts.init(ss.x, ss.y)
    
    lcd.clear(lcd.COLOR_WHITE)
end

function draw_header()
    lcd.set_text_color(lcd.COLOR_BLACK)
    lcd.set_back_color(lcd.COLOR_WHITE)
    lcd.display_string_at(0, 0,  " <" .. tab .. ">", lcd.CENTER_MODE) 
end

local downloader_context = {
    current_package = "",
    output_file = nil,
    input_topic = "/dev/" .. CONFIG.pkg.proto_mqtt.client.id .. "/pkg/data",
    omit_current = false
}

print("input_topic: " .. downloader_context.input_topic)

function mqtt_data_handler(topic, data, last)
    print("Received data from topic: " .. topic)
    if topic == downloader_context.input_topic then
        if last then
            downloader_context.omit_current = false
            if downloader_context.output_file then
                downloader_context.output_file:write(data)
                downloader_context.output_file:close()
                downloader_context.output_file = nil
            end

            packages_list = pkg.get_packages_list()
            pkg_list_context = { elements = create_packages_list(packages_list) }

            return
        elseif downloader_context.omit_current then
            return
        elseif downloader_context.output_file == nil then
            local s, e = string.find(data, ";", 1)

            if s == nil then
                downloader_context.omit_current = true
                return
            end

            local output_file_path = pkg.get_packages_dir() .. '/' .. string.sub(data, 1, s - 1)
            print("Saving file to: " .. output_file_path)
            downloader_context.output_file = io.open(output_file_path, "wb")

            downloader_context.output_file:write(string.sub(data, e + 1))

        elseif downloader_context.output_file ~= nil then
            downloader_context.output_file:write(data)
            downloader_context.output_file:flush()
        end
    else
        print("[MQTT] Received data: `" .. data .. '`')
    end
end

function mqtt_connection_callback(status)
    if status == 0 then
        client:setIncomingDataHandler(mqtt_data_handler)
        print("Subscribe status: " .. tostring(client:subscribe(downloader_context.input_topic, 1)))
    else
        print("MQTT connection failure")
    end
end

local network_initialized = false

function connect_and_start_client(_)
    if not network_initialized then
        lcd.set_text_color(lcd.COLOR_BLACK)
        lcd.set_back_color(lcd.COLOR_WHITE)
        lcd.display_string(2, "Initializing network...")
        lcd.swap_buffers()
        
        netif.init()
        if CONFIG.network.use_dhcp then
            netif.dhcp_client_enable()
        else
            netif.set_ip4_configuration(CONFIG.network.static)
        end
        network_initialized = true
    end
    if not client:isConnected() then
        print("Connect status: " .. tostring(client:connect(CONFIG.pkg.proto_mqtt.server.ip, CONFIG.pkg.proto_mqtt.server.port, CONFIG.pkg.proto_mqtt.client, mqtt_connection_callback)))
    end
end

local download_gui = {
    elements = {
        { class = "text", line = 2, text = function() if netif.is_link_up() then return ("IP: " .. netif.get_ip4()) else return "" end end, text_color = lcd.COLOR_BLACK, back_color = lcd.COLOR_WHITE },
        { class = "text", line = 3, 
            text =
                function() 
                    if client:isConnected() then
                        return ("MQTT: " .. CONFIG.pkg.proto_mqtt.client.id .. "@" .. CONFIG.pkg.proto_mqtt.server.ip .. ":" .. CONFIG.pkg.proto_mqtt.server.port)
                    else return "" end
                end, 
            text_color = lcd.COLOR_BLACK, back_color = lcd.COLOR_WHITE },
        { class = "button", name = "connect_button", x = 350, y = 30, width = 120, height = 40, text = "Connect", frame_color = lcd.COLOR_BLACK, inner_color = lcd.COLOR_GREEN, text_color = lcd.COLOR_BLACK, action = connect_and_start_client},
        { class = "function", fn = draw_header }
    }
}

local apps_gui = {
    elements = {
        { class = "function", fn = draw_header }
    }
}

local packages_gui = {
    elements = {
        { class = "function", fn = draw_header }
    }
}

function remove_app(data)
    pkg.remove_application(data.app_name)
    apps_list = pkg.get_applications_list()
    pkg_list_context = { elements = create_packages_list(packages_list) }
    app_list_context = { elements = create_applications_list(apps_list) }
end

function install_application_if_not_exists(data)
    local app_name = data.app_name
    
    if pkg.application_is_unpacked(app_name) then return end
    
    pkg.unpack_package(app_name)
    
    apps_list = pkg.get_applications_list()
    
    pkg_list_context = { elements = create_packages_list(packages_list) }
    app_list_context = { elements = create_applications_list(apps_list) }
end

function remove_package(data)
    pkg.remove_package(data.app_name .. ".zip")
    packages_list = pkg.get_packages_list()
    pkg_list_context = { elements = create_packages_list(packages_list) }
end


function create_applications_list(list)
    local line_size = lcd.get_font_size()
    
    local elements_list = {}
    
    for i,app_name in ipairs(list) do
        local k = i + 1
        table.insert(elements_list, { class = "text", name = "app_list_text-" .. i, line = k, text = i .. ") " .. app_name})
        
        table.insert(elements_list, { class = "button", name = "app_list_launch_button-" .. i, x = 448, y = k * line_size - 1, width = line_size, height = line_size + 2, text = "+", inner_color = lcd.COLOR_GREEN, action = function(x) system.launch_application(x.app_name) end, action_args = { ["app_name"] = app_name } })
        
        table.insert(elements_list, { class = "button", name = "app_list_remove_button-" .. i, x = 408, y = k * line_size - 1, width = line_size, height = line_size + 2, text = "x", inner_color = lcd.COLOR_LIGHTRED, action = remove_app, action_args = { ["app_name"] = app_name } })
    end
    
    return elements_list
end

function create_packages_list(list)
    local line_size = lcd.get_font_size()
    
    local elements_list = {}
    
    for i,pkg_name in ipairs(list) do
        local k = i + 1
        table.insert(elements_list, { class = "text", name = "pkg_list_text-" .. i, line = k, text = i .. ") " .. pkg_name})
        if not pkg.application_is_unpacked(pkg_name) then
            table.insert(elements_list, { class = "button", name = "pkg_list_install_button-" .. i, x = 448, y = k * line_size - 1, width = line_size, height = line_size + 2, text = "+", inner_color = lcd.COLOR_GREEN, action = install_application_if_not_exists, action_args = { ["app_name"] = pkg_name } })
        end
        
        table.insert(elements_list, { class = "button", name = "pkg_list_remove_button-" .. i, x = 408, y = k * line_size - 1, width = line_size, height = line_size + 2, text = "x", inner_color = lcd.COLOR_LIGHTRED, action = remove_package, action_args = { ["app_name"] = pkg_name } })
    end
    
    return elements_list
end

function process_apps_tab()
    lcd.clear(lcd.COLOR_WHITE)
    
    gui.draw_gui({apps_gui, app_list_context})
    
    lcd.swap_buffers()
    
    gui.process_gui({apps_gui, app_list_context})
    
    system.delay(10)
end

function process_packages_tab()
   lcd.clear(lcd.COLOR_WHITE)
   
   gui.draw_gui({packages_gui, pkg_list_context})
   
   lcd.swap_buffers()
   
   gui.process_gui({packages_gui, pkg_list_context})
   
   system.delay(10)
end

function process_download_tab()
    lcd.clear(lcd.COLOR_WHITE)
    
    gui.draw_gui({download_gui})
    lcd.swap_buffers()
    
    gui.process_gui({download_gui})

    system.delay(10)
end

function next_tab(current_tab)
    if current_tab == "apps" then return "packages" end
    if current_tab == "packages" then return "download" end
    if current_Tab == "download" then return "apps" end
    return "apps"
end

function main()
    print("System starting - Package Manager Gui")
    
    pkg.init(CONFIG.pkg.dirs.packages, CONFIG.pkg.dirs.apps)
    
    btn.init()
    
    init_display()
    lcd.set_font_size(20)
    lcd.swap_buffers()
    lcd.set_font_size(20)
    
    while not netif.is_link_up() do
       system.delay(100) 
    end
    
    local was_pressed = false
    
    apps_list = pkg.get_applications_list()
    packages_list = pkg.get_packages_list()

    pkg_list_context = { elements = create_packages_list(packages_list) }
    app_list_context = { elements = create_applications_list(apps_list) }
    
    while true do
        if btn.is_pressed() and not was_pressed then
           was_pressed = true
           tab = next_tab(tab)
        elseif not btn.is_pressed() and was_pressed then
           was_pressed = false 
        end
        
        if tab == "apps" then
            process_apps_tab()
        elseif tab == "packages" then
            process_packages_tab()
        elseif tab == "download" then
            process_download_tab()
        end
    end
end

main()
